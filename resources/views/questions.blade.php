<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Assessment</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="row justify-content-center">
            <div class="col-md-8">
                    <div class="h3 mb-4">Contesta las siguientes preguntas</div>

                    <div>
                        <form method="POST" action="{{ route('answers') }}">
                            @csrf

                           @foreach($questions as $k => $q)
                               <div class="form-group row">
                                   <label for="q-{{$q->id}}" class="col-12">{{ ($k+1).'. '.$q->question }}</label>
                                   <div class="col-12">
                                   @if($q->type == 'open')
                                       <input type="text" id="q-{{$q->id}}" name="q-{{$q->id}}" class="col-6" required>
                                   @elseif($q->type == 'multiple')
                                       <?php
                                           $opts = explode(',', $q->options);
                                       ?>
                                       @foreach($opts as $o)
                                           <input type="checkbox" name="q-{{$q->id}}[]" class="ml-4 chk" value="{{ $o }}"> {{ $o }}
                                       @endforeach
                                   @elseif($q->type == 'select')
                                       <?php
                                            $opts = explode(',', $q->options);
                                       ?>
                                       @foreach($opts as $o)
                                           <input type="radio" name="q-{{$q->id}}" class="ml-4" value="{{ $o }}" required> {{ $o }}
                                       @endforeach
                                   @endif
                                   @if ($errors->has('name'))
                                   <span class="invalid-feedback" role="alert">
                                       <strong>{{ $errors->first('q-'.$q->id) }}</strong>
                                   </span>
                                   @endif
                                   </div>
                               </div>
                           @endforeach

                            <div class="form-group row mb-0">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary">
                                        Guardar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
            </div>
    </div>

    <script href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>

    </body>
</html>
