<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Assessment</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="row justify-content-center">
            <div class="col-md-8">
                    <div class="h3 mb-4">Respuestas</div>

                    <div>
                        @foreach($users as $u)
                            <div class="card">
                                <div class="card-header">{{ $u->name }} ({{ $u->email }})</div>
                                <div class="card-body">
                                    @foreach($questions as $k => $q)
                                        <p>{{ $q->question }}</p>
                                        <p>{{ $u->answers[$k]->answer }}</p>
                                    @endforeach
                                </div>
                            </div>
                            @endforeach
                    </div>
            </div>
    </div>

    <script href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>

    </body>
</html>
