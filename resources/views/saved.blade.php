<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Assessment</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
    <div class="container">
        <div class="row justify-content-center">
           <h1>Tus respuestas fueron guardadas.</h1>
        </div>
        <div class="row justify-content-center">
            <a href="{{ route('home') }}">Regresar al inicio.</a>
        </div>
    </div>

    <script href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
    </body>
</html>
