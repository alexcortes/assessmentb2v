## Instrucciones

1. Clonar repositorio => git clone https://bitbucket.org/alexcortes/assessmentb2v/
2. Instalar dependencias => composer install
3. Crear una base de datos llamada assesment
4. Crear archivo .env con la configuracion de la base de datos
5. Correr las migraciones => php artisan migrate
6. Correr los seeds de la base de datos => php artisan db:seed

