<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 9/03/19
 * Time: 01:46 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'question_id', 'answer'
    ];
}