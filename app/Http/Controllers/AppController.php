<?php

namespace App\Http\Controllers;


use App\Models\Answer;
use App\Models\Question;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


class AppController extends Controller
{

    public function getQuestions(Request $request) {
        $v = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255']
        ]);

        $v->validate();


        $u = User::create([
            'name' => $request->name,
            'email' => $request->email
        ]);

        Session::put('uid', $u->id);

        $q = Question::all();

        return view('questions', ['questions' => $q]);

    }

    public function answerQuestions(Request $request) {

        $questions = Question::all();
        $arr = [];
        foreach($questions as $q) {
            $arr['q-'.$q->id] = ['required'];
        }

        $v = Validator::make($request->all(), $arr);
        if($v->fails()) {
            dd('Todas la respuestas son requeridas.');
        }

        DB::beginTransaction();
        try {
            $req = $request->all();
            unset($req['_token']);
            foreach ($req as $k => $r) {
                if(is_array($r))
                    $r = implode(',', $r);

                Answer::create([
                    'user_id' => session('uid'),
                    'question_id' => substr($k, 2),
                    'answer' => $r
                ]);
            }
            DB::commit();

            return view('saved');
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e);
        }
    }

    public function getAnswers() {
        $users = User::whereHas('answers')->with('answers')->get();
        $q = Question::all();
        return view('show', ['questions' => $q, 'users' => $users]);
    }
}
