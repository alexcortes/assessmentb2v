<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        DB::table('questions')->insert([
            'question' => 'Elige tus deportes favoritos.',
            'type' => 'multiple',
            'options' => 'Soccer,Basketball,Football,Baseball,Volleyball,Tenis,Golf,Hockey,Rugby,Boxing',
        ]);

        DB::table('questions')->insert([
            'question' => 'Menciona cuales son tus equipos o deportistas favoritos.',
            'type' => 'open',
            'options' => '',
        ]);

        DB::table('questions')->insert([
            'question' => '¿Cuantos dias a la semana practicas algun deporte?',
            'type' => 'select',
            'options' => '0,1-2,3-4,5+',
        ]);

        DB::table('questions')->insert([
            'question' => '¿En que canales de TV ves deportes?',
            'type' => 'multiple',
            'options' => 'Televisa,Imagen,TV Azteca,ESPN,Fox Sports,TDN',
        ]);

        DB::table('questions')->insert([
            'question' => '¿Que equipo crees que gane el proximo superbowl?',
            'type' => 'select',
            'options' => '49ers,Steelers,Cowboys,Patriots,Chiefs,Rams',
        ]);
    }
}
